# Build stage
FROM node:16.20.2-alpine3.18 AS build

## Set current working directory
WORKDIR /build

## Copy the dockerfile's context's community server files
COPY . .

## cat the test-services-jtech-se.pem file and append it to the trusted certificates file
RUN cat test-services-jtech-se.pem >> /etc/ssl/certs/ca-certificates.crt &&\
## Install packages to node_modules
   npm install -g npm@9.8.1 &&\
   npm ci --unsafe-perm

# Create image
FROM node:16.20.2-alpine3.18

## Container config & templates for 
# RUN mkdir /config /templates

## Set current directory
WORKDIR /community-server

COPY --from=build /build/AdminAPI.yaml .
COPY --from=build /build/package.json .
COPY --from=build /build/config ./config
COPY --from=build /build/templates ./templates
COPY --from=build /build/node_modules ./node_modules
COPY --from=build /build/dist ./dist
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

## Informs Docker that the container listens on the specified network port at runtime
EXPOSE 3000

## Set command run by the container
ENTRYPOINT [ "npx", "community-solid-server" ]
