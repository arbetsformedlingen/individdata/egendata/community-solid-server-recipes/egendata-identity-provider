import { GetSessionParams, GetSessionResponse, LoginParams, LoginResponse, LogoutParams, LogoutResponse, SignParams, SignResponse, VismaClientSettings, VismaClientType, VismaRoute } from "./VismaClient";
import { TestLoginHandler } from "./TestLoginHandler";
import { reduce } from "@solid/community-server";

//
// Mock Client implementation
//

export class VismaClientMock implements VismaClientType {
  constructor(private readonly options: VismaClientSettings, private readonly handler: TestLoginHandler) {}

  async login(parameters: LoginParams) {
    const returnUrl = parameters.callbackUrl;
    const sessionId = this.handler.login(returnUrl);
    const redirectUrl = new URL(this.options.apiUrl)
    redirectUrl.searchParams.append('sessionid', sessionId);
    const resp: LoginResponse = {
      redirectUrl: redirectUrl.toString(),
      sessionId,
    }
    return resp;
  }

  async sign(parameters: SignParams) {
    const resp: SignResponse = {
      redirectUrl: parameters.callbackUrl,
      sessionId: '00000000'
    }
    return resp;
  }

  async getSession(parameters: GetSessionParams) {
    const resp = this.handler.getSession(parameters.sessionId);
    return resp;
  }

  async logout(parameters: LogoutParams) {
    const resp: LogoutResponse = {
      sessionDeleted: 0,
    }
    return resp;
  }
}

