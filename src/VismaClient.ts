import fetch from 'node-fetch';
import HttpsProxyAgent from 'https-proxy-agent';


export interface CallResponse {
  redirectUrl: string;
  sessionId: string;
}

//
// Type definitions for /json1.1/Login
//

export interface LoginParams {
  callbackUrl: string
  relayState?: string;
}

export interface LoginResponse extends CallResponse {}

//
// Type definitions for /json1.1/Sign
//

export interface SignParams { 
  callbackUrl: string
  userVisibleData: string;
  userNonVisibleData?: string;
  personalNumber?: string;
  relayState?: string; 
}

export interface SignResponse extends CallResponse {}

//
// Type definitions for /json1.1/GetSession
//

export interface GetSessionParams {
  sessionId: string;
  logout?: boolean;
}

export interface BankIdAttributes {
  C: string;
  CN: string;
  G?: string;
  GN?: string;
  O: string;
  SN: string;
  idp: string;
  issuerCommonName: string;
  name: string;
  serialNumber: string;
  system: string;
  type: string;
}

export interface FrejaAttributes {
  C: string;
  CN: string;
  G?: string;
  GN?: never;
  SN: string;
  dateOfBirth: string;
  email: string;
  idp: string;
  issuerCommonName: string;
  name: string;
  serialNumber: string;
  system: string;
  type: string;
  urn_oid_1_2_752_201_3_2: string;
  urn_oid_1_2_752_201_3_3: string;
}

export type UserAttributes =  BankIdAttributes | FrejaAttributes;

export interface GetSessionResponse {
  sessionId: string;
  userAttributes: UserAttributes;
  username: string;
}

//
// Type definitions for /json1.1/Logout
//

export interface LogoutParams {
  sessionId: string;
}

export interface LogoutResponse {
  sessionDeleted: number;
}

//
// Type definitions for error responses
//

export interface ErrorObject {
  code: string;
  message: string;
}

export interface ErrorResponse {
  errorObject: ErrorObject;
}

//
// Client settings
//

export interface VismaClientSettings {
  apiUrl: string;
  customerKey: string;
  serviceKey: string;
  proxyUrl?: string;
}

//
// Error types
//

export class VismaError {
  readonly name: string;
  readonly code: string;
  readonly message: string;

  constructor(code: string, message: string) {
    Error.captureStackTrace(this, this.constructor);

    this.name = "VismaError";
    this.code = code;
    this.message = message;
  }
}

export enum VismaRoute {
  login = "/json1.1/Login",
  sign = "/json1.1/Sign",
  session = "/json1.1/GetSession",
  logout = "/json1.1/Logout",
}

export interface VismaClientType {
  login(parameters: LoginParams): Promise<LoginResponse>,
  sign: (parameters: SignParams) => Promise<SignResponse>,
  logout: (parameters: LogoutParams) => Promise<LogoutResponse>,
  getSession: (parameters: GetSessionParams) => Promise<GetSessionResponse>
};

//
// Client implementation
//

export class VismaClient implements VismaClientType {
  readonly baseUrl: string;
  readonly customerKey: string;
  readonly serviceKey: string;
  readonly proxyUrl?: string;

  constructor(options: VismaClientSettings) {
      this.baseUrl = options.apiUrl;
      this.customerKey = options.customerKey;
      this.serviceKey = options.serviceKey;
      this.proxyUrl = options.proxyUrl;
  }

  login(parameters: LoginParams): Promise<LoginResponse> {
    return this._call<LoginParams, LoginResponse>(VismaRoute.login, parameters);
  }

  sign(parameters: SignParams): Promise<SignResponse> {
    if (!parameters.userVisibleData) {
      throw Error("Missing required arguments: userVisibleData.");
    }

    parameters = {
      ...parameters,
      userVisibleData:  btoa(parameters.userVisibleData),
      userNonVisibleData: parameters.userNonVisibleData
        ? btoa(parameters.userNonVisibleData)
        : undefined,
    };

    return this._call<SignParams, SignResponse>(VismaRoute.sign, parameters);
  }

  getSession(parameters: GetSessionParams) {
    return this._call<GetSessionParams, GetSessionResponse>(
      VismaRoute.session,
      parameters,
    );
  }

  logout(parameters: LogoutParams): Promise<LogoutResponse> {
    return this._call<LogoutParams, LogoutResponse>(VismaRoute.logout, parameters);
  }

  _call<Req, Res>(
    route: VismaRoute,
    payload: Req,
  ): Promise<Res> {
    return new Promise((resolve, reject) => {
      const visma = new URL(route, this.baseUrl);
      visma.searchParams.set('customerKey', this.customerKey);
      visma.searchParams.set('serviceKey', this.serviceKey);
      const entries = Object.entries(payload as Object);
      for (const [key, value] of entries) {
        visma.searchParams.set(key, value);
      }
      const options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        agent: this.proxyUrl ? HttpsProxyAgent(this.proxyUrl) : undefined,
      };
      fetch(visma, options)
        .then(response => response.json())
        .then(data => { resolve(data); })
        .catch(err => {
          console.log('Error occurred when calling BankID, error:', err);
          reject(err);
        });

    });
  }
}
