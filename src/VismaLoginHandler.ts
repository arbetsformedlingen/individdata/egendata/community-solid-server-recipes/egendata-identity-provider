import { APPLICATION_JSON, BadRequestHttpError, BaseInteractionHandler, BasicRepresentation, FoundHttpError, getLoggerFor, InteractionHandlerInput, ProviderFactory, readJsonStream, Representation } from '@solid/community-server';
import type { InteractionResults } from 'oidc-provider';
import { v4 } from 'uuid';
import { UserAttributes, VismaClient, VismaClientType } from './VismaClient';
import { PersonRegistrationParams, RegistrationManager } from './RegistrationManager';
import { AccountStore } from './AccountStore';

//
// Login Handler settings
//

interface VismaLoginHandlerSettings {
  providerFactory: ProviderFactory;
  accountStore: AccountStore;
  vismaClient: VismaClientType;
  registrationManager: RegistrationManager;
}


/**
 * Handles the redirection to VISMA.
 */
export class VismaLoginHandler extends BaseInteractionHandler {
  protected readonly logger = getLoggerFor(this);

  private readonly accountStore: AccountStore;
  private readonly vismaClient: VismaClientType;
  private readonly registrationManager: RegistrationManager;

  public constructor(settings: VismaLoginHandlerSettings) {
    super({});
    this.accountStore = settings.accountStore;
    this.vismaClient = settings.vismaClient;
    this.registrationManager = settings.registrationManager;
  }

  public async canHandle(input: InteractionHandlerInput): Promise<void> {
    await super.canHandle(input);
    if (input.operation.method === 'POST' && !input.oidcInteraction) {
      throw new BadRequestHttpError(
        'This action can only be performed as part of an OIDC authentication flow.',
        { errorCode: 'E0002' },
      );
    }
  }

  protected async handleGet(input: Required<InteractionHandlerInput>): Promise<Representation> {
    const { operation, oidcInteraction } = input;


    // Initiate login at Visma
    const prom = this.vismaClient.login({
      callbackUrl: operation.target.path,
    });
    const loginResponse = await prom;

    if (!loginResponse.sessionId) {
      // Visma did not return a session id
      this.logger.error(`Error from Visma: ${(loginResponse as any).errorObject.message}`)
      throw new BadRequestHttpError(`Error from Visma: ${(loginResponse as any).errorObject}`);
    }

    return new BasicRepresentation(JSON.stringify(loginResponse), operation.target, APPLICATION_JSON);
  }

  public async handlePost({ operation, oidcInteraction }: InteractionHandlerInput): Promise<never> {
    const { sessionId } = await readJsonStream(operation.body.data);
    // get session data from Visma
    const sessionResponse = await this.vismaClient.getSession({ sessionId });
    if (!sessionResponse.sessionId) {
      // Visma did not return a session id
      this.logger.error(`Error from Visma: ${(sessionResponse as any).errorObject.message}`)
      throw new BadRequestHttpError(`Error from Visma: ${(sessionResponse as any).errorObject}`);
    }

    const webId = await this.getWebId(sessionResponse.userAttributes, this.accountStore, this.registrationManager, v4);

    // Update the interaction to get the redirect URL
    const login: InteractionResults['login'] = {
      accountId: webId,
      remember: false,
    };
    oidcInteraction!.result = { login };
    await oidcInteraction!.save(oidcInteraction!.exp - Math.floor(Date.now() / 1000));

    throw new FoundHttpError(oidcInteraction!.returnTo);
  }

  private async getWebId(userAttributes: UserAttributes, accountStore: AccountStore, registrationManager: RegistrationManager, uuid: typeof v4): Promise<string> {
    const givenName = userAttributes.G ?? userAttributes.GN ?? '';
    const registrationParams: PersonRegistrationParams =  {
      t: 'person',
      personalNumber: userAttributes.serialNumber,
      podName: uuid(),
      firstName: givenName,
      lastName: userAttributes.SN,
    }
    const ssn = userAttributes.serialNumber;
    const accountInfo = await accountStore.getAccountInfo(ssn);
    if (accountInfo) {
      // account previously registered
      const settings = await accountStore.getSettings(accountInfo.webId);
      if (!settings.storageCreated) {
        // storage could previously not be created, try again
        registrationManager.createPersonStorage(accountInfo.webId, settings.podStorage!);
      }
      return accountInfo.webId;
    }

    // first time login - register account
    const registrationResponse = await registrationManager.register(registrationParams, false);
    return registrationResponse.webId!;
  }
  
}

