import { HttpHandler, HttpHandlerInput, HttpRequest, RepresentationMetadata, ResponseDescription, ResponseWriter, SOLID_HTTP } from "@solid/community-server";
import { DataFactory } from 'n3';
import { v4 } from 'uuid';
import { GetSessionResponse } from './VismaClient';
import arrayifyStream from 'arrayify-stream';
import { parse } from 'querystring';


export class TestLoginHandler extends HttpHandler {

  private readonly sessions: Record<string, GetSessionResponse>;

  constructor(
    private readonly responseWriter: ResponseWriter
  ) {
    super();
    this.sessions = {};
  };

  login(returnUrl: string) {
    const sessionId = v4();
    return sessionId;
  }

  getSession(sessionId: string): GetSessionResponse {
    const sessionResponse = this.sessions[sessionId];
    delete this.sessions[sessionId];
    return sessionResponse;
  }

  private readonly htmlForm = (sessionId: string | null) => `
  <html>

  <head>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cabin:ital,wght@1,700&family=Roboto&display=swap"
      rel="stylesheet">
  
    <style>
      * {
        box-sizing: border-box;
      }
  
      body {
        font-family: 'Roboto', sans-serif;
        font-size: 1.2rem;
        color: rgb(30, 30, 30);
      }
  
      #container {
        min-width: 600px;
        width: 50vw;
        margin: 10vh auto 0 auto;
        background-color: rgba(30, 30, 30, 0.02);
        padding: 1em 2em;
        border-radius: 0.5em;
        border: 0.1em solid rgba(30, 30, 30, 0.1);
        box-shadow: 0.1em 0.1em 0.3em rgba(30, 30, 30, 0.1);
      }
  
      h1 {
        font-size: 6em;
        text-align: center;
        margin: 0.3333em auto 0.6667em auto;
      }
  
      h1 span.sub {
        font-size: 0.25em;
        font-weight: 400;
        color: rgba(30, 30, 30, 0.8)
      }
  
      h1 span.logo {
        font-family: 'Cabin', sans-serif;
        font-style: italic;
        font-weight: 700;
        color: rgb(24, 62, 79);
      }
  
      form>div {
        width: 60%;
        margin: 2em auto;
      }
  
      input,
      textarea {
        font-family: inherit;
        font-size: inherit;
      }
  
      label,
      input {
        display: block;
        width: 100%;
      }
  
      input {
        padding: 0.5em;
        margin: 0.5em 0;
        border: none;
        border-radius: 0.2em;
        box-shadow: 0.1em 0.1em 0.3em rgba(30, 30, 30, 0.1);
      }
  
      input[type=submit] {
        appearance: none;
        color: rgb(255, 255, 255);
        background-color: rgb(24, 62, 79);
        font-size: 1.5em;
      }
  
      label {
        color: rgba(30, 30, 30, 0.8);
        font-style: italic;
        margin-bottom: -0.5em;
      }
    </style>
  </head>
  
  <body>
    <div id="container">
  
      <h1><span class="sub">Sign in with</span><br /><span class="logo">FakeID</span></h1>
      <form action="/.login/" method="POST">
        <input type="hidden" id="sessionid" name="sessionid" value="${sessionId}" />
        <div>
          <label for="gname">Given Name</label>
          <input type="text" id="gname" name="gname" value="Test" />
        </div>
        <div>
          <label for="fname">Family name</label>
          <input type="text" id="fname" name="fname" value="Person" />
        </div>
        <div>
          <label for="pnr">Identity number (12 digits)</label>
          <input type="text" id="pnr" name="pnr" value="200402185441" />
        </div>
        <div>
          <input type="submit" value="SIGN IN">
        </div>
      </form>
  
    </div>
  </body>
  
  </html>
  `;

  async handle({ request, response }: HttpHandlerInput) {
    const method = request.method;
    if (method === 'GET') {
      const searchParams = new URLSearchParams(request.url?.split('?').pop())
      const sessionId = searchParams.get('sessionid');
      response.end(this.htmlForm(sessionId));
    } else if (method === 'POST') {

      const body = await readBody(request);

      const name = `${body.gname} ${body.fname}`;
      const getSessionResponse = {
        sessionId: body.sessionid,
        username: name,
        userAttributes: {
          C: '',
          CN: '',
          G: body.gname,
          GN: body.gname,
          O: 'Test Organization',
          SN: body.fname,
          idp: 'Test IDP',
          issuerCommonName: 'Test Issuer',
          name,
          serialNumber: body.pnr,
          system: 'Test',
          type: 'B',
        }
      }

      this.sessions[body.sessionid] = getSessionResponse;

      const metadata = new RepresentationMetadata({ [SOLID_HTTP.location]: DataFactory.namedNode(`/idp/login/?ts_session_id=${body.sessionid}`) });
      const result = new ResponseDescription(302, metadata);
      await this.responseWriter.handleSafe({ response, result });
    }
  }
}

export type Person = {
  gname: string,
  fname: string,
  pnr: string,
  sessionid: string,
}

const readBody = async (request: HttpRequest): Promise<Person> => {
  const body = (await arrayifyStream<string>(request)).join('');
  const person = parse(body);
  return person as Person;
}