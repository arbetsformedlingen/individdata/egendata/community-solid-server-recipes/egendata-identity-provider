import { KeyValueStorage } from '@solid/community-server';
import assert from 'assert';
import { hash, compare } from 'bcryptjs';
import { v4 } from 'uuid';
import type { AccountInfo, AccountSettings, AccountStore } from './AccountStore';

export type AccountData = AccountInfo | AccountSettings;

/**
 * An AccountStore that uses a KeyValueStorage to persist a WebId (and the underlying generated uuid), and settings for a given SSN.
 */
export class SsnAccountStore implements AccountStore {
  private readonly storage: KeyValueStorage<string, AccountData>;

  public constructor(storage: KeyValueStorage<string, AccountData>) {
    this.storage = storage;
  }

  /**
   * Generates a ResourceIdentifier to store data for the given ssn.
   */
  private getAccountResourceIdentifier(ssn: string): string {
    return `account/${encodeURIComponent(ssn)}`;
  }

  public async getAccountInfo(ssn: string): Promise<AccountInfo | undefined> {
    const key = this.getAccountResourceIdentifier(ssn);
    const accountInfo = await this.storage.get(key) as AccountInfo | undefined;
    return accountInfo;
  }

  public async create(ssn: string | undefined, webId: string, uuid: string, settings: AccountSettings): Promise<void> {
    if (await this.storage.get(webId)) {
      //There is already an account for this WebID
      return;
    }
    if (ssn) {
      const key = this.getAccountResourceIdentifier(ssn);
      const payload: AccountInfo = {
        uuid,
        webId,
      };
      await this.storage.set(key, payload);
    
    }
    await this.storage.set(webId, settings);
  }

  public async getSettings(webId: string): Promise<AccountSettings> {
    const settings = await this.storage.get(webId) as AccountSettings | undefined;
    assert(settings, 'Account does not exist');
    return settings;
  }

  public async updateSettings(webId: string, settings: AccountSettings): Promise<void> {
    const oldSettings = await this.storage.get(webId);
    assert(oldSettings, 'Account does not exist');
    await this.storage.set(webId, settings);
  }

  public async deleteAccount(ssn: string | undefined, webId: string): Promise<void> {

    if (ssn) {
      const key = this.getAccountResourceIdentifier(ssn);
      await this.storage.delete(key);
    }
    await this.storage.delete(webId);
  }
}
