import { v4 as uuid } from 'uuid';
import { SignJWT, importJWK } from 'jose';
import { ClientCredentials, getLoggerFor, HttpError, IdentifierGenerator, joinUrl, JwkGenerator, KeyValueStorage } from '@solid/community-server';
import type { Logger } from '@solid/community-server';
import { ProfilePodSettings } from './ProfilePodSettings';
import { RemovablePodManager } from './RemovableGeneratedPodManager';
import { AccountType, RegistrationManager, RegistrationParams, RegistrationResponse, RootRegistrationParams } from './RegistrationManager';
import { AccountSettings, AccountStore } from './AccountStore';
import { AuthFetchFactory, AuthFetchFunction } from './AuthFetchFactory';
import { assert } from 'console';

export interface ProfileRegistrationManagerArgs {
  /**
   * The root webId used for registering pods.
   */
  rootWebId: string;
  /**
   * Base URL for the server.
   */
  baseUrl: string;
  /**
   * Appended to the generated pod identifier to create the corresponding WebID.
   */
  webIdSuffix: string;
  /**
   * Generates identifiers for new pods.
   */
  identifierGenerator: IdentifierGenerator;
  /**
   * Stores all the registered account information.
   */
  accountStore: AccountStore;
  /**
   * Creates the new pods.
   */
  rootPodManager: RemovablePodManager;
  /**
   * Creates the new pods.
   */
  servicePodManager: RemovablePodManager;
  /**
   * Creates the new pods.
   */
  personPodManager: RemovablePodManager;
  /**
   * Base url of other pod storage server
   */
  podStorageBaseUrl?: string;
  /**
   * Credential storage
   */
  credentialStorage: KeyValueStorage<string, ClientCredentials>,
  /**
   * Client id for this service
   */
  clientId: string,
  /**
   * Client secret for this service
   */
  clientSecret: string,
  /**
   * Used to generate and cache/store JWKS
   */
  jwkGenerator: JwkGenerator;
  /**
   * Factory to create an authenticated fetch
   */
  authFetchFactory: AuthFetchFactory;
}

/**
 * Supports IDP registration and pod creation based on input parameters.
 *
 * The above behaviour is combined in the two class functions.
 * `validateInput` will make sure all incoming data is correct and makes sense.
 * `register` will call all the correct handlers based on the requirements of the validated parameters.
 */
export class ProfileRegistrationManager implements RegistrationManager {
  protected readonly logger = getLoggerFor(this);

  private readonly jwtAlg = 'ES256';

  private readonly rootWebId: string;
  private readonly baseUrl: string;
  private readonly webIdSuffix: string;
  private readonly identifierGenerator: IdentifierGenerator;
  private readonly accountStore: AccountStore;
  private readonly rootPodManager: RemovablePodManager;
  private readonly servicePodManager: RemovablePodManager;
  private readonly personPodManager: RemovablePodManager;
  private readonly podStorageBaseUrl: string | undefined;
  private readonly credentialStore: KeyValueStorage<string, ClientCredentials>;
  private readonly clientId: string;
  private readonly clientSecret: string;
  private readonly jwkGenerator: JwkGenerator;
  private readonly authFetchFactory: AuthFetchFactory;

  public constructor(args: ProfileRegistrationManagerArgs) {
    this.rootWebId = args.rootWebId;
    this.baseUrl = args.baseUrl;
    this.webIdSuffix = args.webIdSuffix;
    this.identifierGenerator = args.identifierGenerator;
    this.accountStore = args.accountStore;
    this.rootPodManager = args.rootPodManager;
    this.servicePodManager = args.servicePodManager;
    this.personPodManager = args.personPodManager;
    this.podStorageBaseUrl = args.podStorageBaseUrl;
    this.credentialStore = args.credentialStorage;
    this.clientId = args.clientId;
    this.clientSecret = args.clientSecret;
    this.jwkGenerator = args.jwkGenerator;
    this.authFetchFactory = args.authFetchFactory;
  }

  public async registerRoot(rootRegistrationParams: RootRegistrationParams) {
    // Temporary code to add this identity provider identity
    const registrationResponse = await this.register(rootRegistrationParams);

    const settings = await this.accountStore.getSettings(registrationResponse.webId);

    // Store the credentials, and point to them from the account
    settings.clientCredentials = settings.clientCredentials ?? [];
    const clientCredentialsSet = new Set(settings.clientCredentials);
    clientCredentialsSet.add(this.clientId);
    settings.clientCredentials = [...clientCredentialsSet];
    await this.accountStore.updateSettings(registrationResponse.webId, settings);
    await this.credentialStore.set(this.clientId, { secret: this.clientSecret, webId: registrationResponse.webId });
  };

  /**
   * Handles the 3 potential steps of the registration process:
   *  1. Generating a new WebID.
   *  2. Registering a WebID with the IDP.
   *  3. Creating a new pod for a given WebID.
   *
   * All of these steps are optional and will be determined based on the input parameters.
   *
   * This includes the following steps:
   *  * When registering and creating a pod, the base URL will be used as oidcIssuer value.
   */
  public async register(input: RegistrationParams, allowRoot = false): Promise<RegistrationResponse> {
    assert(this.identifierGenerator)

    const podBaseUrl = this.identifierGenerator.generate(input.podName);
    // Create the WebID
    const webId = joinUrl(podBaseUrl.path, this.webIdSuffix);

    // Create the account if it doesn't exist
    const settings: AccountSettings = {
      useIdp: true,
      podBaseUrl: podBaseUrl.path,
      storageCreated: false,
      podStorage: (this.podStorageBaseUrl && (input.t !== 'root')) ? joinUrl(this.podStorageBaseUrl, uuid(), '/') : undefined,
    };

    const personalNumber =  (input.t === 'person') ? input.personalNumber : undefined;

    await this.accountStore.create(personalNumber, webId, input.podName, settings);

    const dataSubjectIdentifierToken = (personalNumber) ? await this.createJWT(webId, personalNumber, this.baseUrl) : undefined;

    let podCreated = true;

    const podSettings: ProfilePodSettings = {
      webId,
      podBaseUrl: podBaseUrl.path,
      pno: personalNumber,
      dsit: dataSubjectIdentifierToken,
      uuid: input.podName,
      name: (input.t === 'service' || input.t === 'root') ? input.name : undefined,
      firstName: (input.t === 'person') ? input.firstName : undefined,
      lastName: (input.t === 'person') ? input.lastName : undefined,
      rootWebId: this.rootWebId,
      logo: (input.t === 'service') ? input.logo : undefined,
    };

    podSettings.podStorage = settings.podStorage;

    podSettings.oidcIssuer = this.baseUrl;

    const usePodManager = (input: RegistrationParams) => {
      switch (input.t) {
        case "root": return this.rootPodManager;
        case "service": return this.servicePodManager;
        case "person": return this.personPodManager;
      }
    }

    const podManager = usePodManager(input);

    try {
      // Only allow overwrite for root pods
      this.logger.debug(`Try to create an identity pod.`);
      await podManager.createPod(podBaseUrl, podSettings, allowRoot);
    } catch (error: unknown) {
      const httpError = error as HttpError;
      if (httpError.statusCode === 409) {
        this.logger.info(`Identity pod creation skipped, resource already exists: ${input.podName}`);
        podCreated = false;
      } else {
        this.logger.warn(`Failed to create identity pod, error: ${error}`);
        throw error;
      }
    }

    if (input.t === 'service' && input.logo) {
      // https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URLs
      const logoUrl = `${this.baseUrl}${input.podName}/profile/logo`;
      const authFetch =  await this.authFetchFactory.getAuthFetch(this.clientId, this.clientSecret);
      await store(logoUrl, input.logo, authFetch, 'text/plain');
      console.log(`Created: ${this.baseUrl}${input.podName}/profile/logo`);
    }


    if (podSettings.podStorage) {
      const authFetch =  await this.authFetchFactory.getAuthFetch(this.clientId, this.clientSecret);
      await this.createStorageImpl(input.t, podSettings.webId, podSettings.podStorage, authFetch);
    }

    return {
      personalNumber: personalNumber,
      webId,
      oidcIssuer: this.baseUrl,
      podBaseUrl: podBaseUrl.path,
      accountCreated: podCreated,
    };
  }

  public async createPersonStorage(webId: string, storagePodUrl: string) {
    const authFetch = await this.authFetchFactory.getAuthFetch(this.clientId, this.clientSecret);
    this.createStorageImpl('person', webId, storagePodUrl, authFetch);
  }

  async createStorageImpl(accountType: AccountType, webId: string, storagePodUrl: string, authFetch: AuthFetchFunction) {
      try {
        // Let's try to create a pod on the other pod storage server
        await this.createStoragePod(accountType, webId, storagePodUrl, authFetch, this.logger);
        let setting = await this.accountStore.getSettings(webId);
        setting.storageCreated = true;
        await this.accountStore.updateSettings(webId, setting);
      } catch(error) {
        this.logger.error(`Failed to create pod on storage server, error: ${error}`);
      }
  }

  private async createStoragePod(
    accountType: AccountType,
    webId: string,
    storagePodUrl: string,
    authFetch: AuthFetchFunction,
    logger: Logger) {
    try {
      //logger.debug(`Try to create a pod in other pod provider: ${storagePodUrl}`);
      //await store(storagePodUrl, emptyContainerTurtle, authFetch);

      const storagePodAclUrl = `${storagePodUrl}.acl`;
      logger.debug(`Try to create pod acl: ${storagePodAclUrl}`);
      await store(storagePodAclUrl, aclTurtle(ownerAclFragment(storagePodUrl, webId), accountType === 'service' ? egendataAclFragment(storagePodUrl, webId) : ''), authFetch);

      if (accountType === 'service') {
        const inboxUrl = `${storagePodUrl}egendata/inbox/`;
        const inboxAclUrl = `${inboxUrl}.acl`;
        await store(inboxAclUrl, aclTurtle(ownerAclFragment(inboxUrl, webId), inboxAclFragment(inboxUrl, webId)), authFetch);
      }
    } catch (err) {
      logger.warn(`Failed to create user pod in the pod provider, error: ${err}`);
      throw err;
    }
  }

  private async createJWT(webid: string, sub: string, issuer: string) {
    const jwkPrivateKey = await this.jwkGenerator.getPrivateKey();
    jwkPrivateKey.alg = this.jwtAlg;
    const privateKey = await importJWK(jwkPrivateKey);
  
    return await new SignJWT({
      webid,
      sub,
    }).setIssuer(issuer)
      .setJti(uuid())
      .setProtectedHeader({
        alg: this.jwtAlg,
      })
      .setIssuedAt()
      .sign(privateKey);
  }
  
}

const profileRegistrationManagerLogger = getLoggerFor('ProfileRegistrationManager');

const store = async (resourceUrl: string, resource: string, authFetch: AuthFetchFunction, contentType = 'text/turtle', logger = profileRegistrationManagerLogger) => {
  logger.verbose(`Trying to create resource: ${resourceUrl} ...`);
  const response = await authFetch(resourceUrl, {
    method: 'PUT',
    headers: {
      'Content-Type': contentType,
    },
    body: resource,
  });
  if (!response.ok) {
    const msg = `Error: ${response.statusText}`;
    logger.verbose(msg);
    throw new Error(msg);
  }
  logger.verbose(`Created: ${resourceUrl}`);
}

const ownerAclFragment = (resource: string, webId: string) =>
`<#owner>
  a acl:Authorization;
  acl:agent <${webId}>;
  acl:accessTo <${resource}>;
  acl:default <${resource}>;
  acl:mode acl:Read, acl:Write, acl:Control.

`;

const egendataAclFragment = (resource: string, rootWebId: string) =>
`<#egendata>
  a acl:Authorization;
  acl:agent <${rootWebId}>;
  acl:accessTo <${resource}>;
  acl:default <${resource}>;
  acl:mode acl:Read, acl:Write, acl:Control.

`;

const inboxAclFragment = (resource: string, webId: string) =>
`<#public>
  a acl:Authorization;
  acl:agentClass foaf:Agent;
  acl:accessTo <${resource}>;
  acl:default <${resource}>;
  acl:mode acl:Write, acl:Append.

`;

const aclTurtle = (ownerFragment: string, egendataFragment: string = '') => 
`@prefix acl: <http://www.w3.org/ns/auth/acl#>.
@prefix foaf: <http://xmlns.com/foaf/0.1/>.
${ownerFragment}${egendataFragment}
`;

const metaTurtle = () => 
`@prefix pim: <http://www.w3.org/ns/pim/space#>.
<> a pim:Storage.`;

