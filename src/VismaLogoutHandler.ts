import { BadRequestHttpError, HttpHandler, HttpHandlerInput } from "@solid/community-server";
import { VismaClient } from "./VismaClient";

export class VismaLogoutHandler extends HttpHandler {
  private readonly vismaClient: VismaClient;

  public constructor(vismaClient: VismaClient) {
    super();
    this.vismaClient = vismaClient;
  }

  public async canHandle(input: HttpHandlerInput): Promise<void> {
    await super.canHandle(input);
    if (input.request.method !== 'POST') {
      throw new BadRequestHttpError(
        'Logout can only be posted to',
        { errorCode: 'E0003' },
      );
    }
  }

  async handle(input: HttpHandlerInput): Promise<void> {
    const body = await input.request.read();
    const { sessionId } = JSON.parse(body.toString());

    if (!sessionId) {
      throw new BadRequestHttpError('No sessionId');
    }
    this.vismaClient.logout({ sessionId });
    input.response.end();
  }
  
}