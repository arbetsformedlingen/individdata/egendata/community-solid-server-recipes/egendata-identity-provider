import { AuthFetchFunction } from "./AuthFetchFactory";

export type AccountType = 'root' | 'person' | 'service';

export type ProviderTemplate = {
  objectType: string,
  documentTitle: string,
}

export type ConsumerTemplate = {
  objectType: string,
  consentText: string,
  consentCheck: string[],
}

export type TemplateContent = ProviderTemplate | ConsumerTemplate;

export type Template = {
  name: string,
  content: TemplateContent,
}

export type RootRegistrationParams  = {
  t: 'root',
  name: string,
  podName: string
}

export type PersonRegistrationParams  = {
  t: 'person',
  podName: string;
  personalNumber: string,
  firstName: string;
  lastName: string;
}

export type ServiceRegistrationParams = {
  t: 'service',
  podName: string;
  name: string;
  clientId: string;
  clientSecret: string;
  logo?: string;
  adminWebId?: string;
}

/**
 * The parameters expected for registration.
 */
export type RegistrationParams = RootRegistrationParams | PersonRegistrationParams | ServiceRegistrationParams;

/**
 * The result of a registration action.
 */
export interface RegistrationResponse {
  personalNumber?: string;
  webId: string;
  oidcIssuer: string;
  podBaseUrl: string;
  accountCreated: boolean;
}

export interface RegistrationManager {
  registerRoot: (input: RootRegistrationParams, registerFunction: RegisterFunction) => Promise<void>;
  register: RegisterFunction;
  createPersonStorage: (webId: string, storagePodUrl: string) => Promise<void>;
}

export type RegisterFunction = (input: RegistrationParams, allowRoot?: boolean) => Promise<RegistrationResponse>;