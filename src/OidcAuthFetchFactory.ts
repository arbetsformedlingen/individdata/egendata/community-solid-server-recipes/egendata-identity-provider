import https from 'https';
import http from 'http';
import nodeFetch from 'node-fetch';
import { buildAuthenticatedFetch, createDpopHeader, generateDpopKeyPair } from "@inrupt/solid-client-authn-core";
import { AuthFetchFactory, FetchFunction, TokenKeyPair } from "./AuthFetchFactory";
import fetch from 'node-fetch';


export class OidcAuthFetchFactory implements AuthFetchFactory {
  private readonly identityServerUrl: string;

  public constructor(baseUrl: string) {
    this.identityServerUrl = baseUrl;
  }
  
  async getAuthFetch(id: string, secret: string): Promise<FetchFunction> {
    const accessTokenKeyPair = await this.fetchAccessTokenKey(id, secret);
    const authFeetch = await this.getAuthFetchFunction(accessTokenKeyPair);
    return authFeetch;
  }

  private async getAuthFetchFunction(arg: TokenKeyPair): Promise<FetchFunction> {
    const {accessToken, dpopKey} = arg;
    const authFetchFunction = await buildAuthenticatedFetch(fetch, accessToken, { dpopKey });
    return authFetchFunction;
  }

  private async fetchAccessTokenKey(myId: string, mySecret: string): Promise<TokenKeyPair> {
    console.log(`Fetching access token ...`);
    const dpopKey = await generateDpopKeyPair();
    const authString = `${encodeURIComponent(myId)}:${encodeURIComponent(mySecret)}`;
    const isHttps = this.identityServerUrl.startsWith('https:');
    const tokenUrl = `${this.identityServerUrl}.oidc/token`;
    const agent = isHttps ? new https.Agent({
      rejectUnauthorized: false,
    }) : new http.Agent();
    const response = await nodeFetch(tokenUrl, {
      method: 'POST',
      headers: {
        // The header needs to be in base64 encoding.
        authorization: `Basic ${Buffer.from(authString).toString('base64')}`,
        'content-type': 'application/x-www-form-urlencoded',
        dpop: await createDpopHeader(tokenUrl, 'POST', dpopKey),
      },
      agent,
      body: new URLSearchParams({
        grant_type: 'client_credentials',
        scope: 'webid'
      })
    });

    const data = await response.json() as any;
    const { access_token } = data;
    return {accessToken: access_token, dpopKey};
  }
}