import { KeyPair } from "@inrupt/solid-client-authn-core";
import { RequestInfo, RequestInit, Response } from "node-fetch";

export type TokenKeyPair = {
  accessToken: string,
  dpopKey: KeyPair,
};

export type FetchFunction = (input: RequestInfo | URL, init?: RequestInit) => Promise<Response>;
export type AuthFetchFunction = (input: RequestInfo | URL, init?: RequestInit | undefined) => Promise<Response>;

export interface AuthFetchFactory {
  getAuthFetch(id: string, secret: string): Promise<FetchFunction>;
}