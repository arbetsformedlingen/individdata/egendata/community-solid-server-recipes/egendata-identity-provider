import { createErrorMessage, ensureTrailingSlash, getLoggerFor, Initializer, joinUrl, KeyValueStorage, ResourceIdentifier, ResourcesGenerator, ResourceStore } from '@solid/community-server';
import { RegistrationManager, RootRegistrationParams } from './RegistrationManager';

export interface RootContainerInitializerArgs {
  /**
   * Base URL of the server.
   */
  baseUrl: string;
  /**
   * Relative path of the container.
   */
  path: string;
  /**
   * ResourceStore where the container should be stored.
   */
  store: ResourceStore;
  /**
   * Generator that should be used to generate container contents.
   */
  generator: ResourcesGenerator;
  /**
   * Key that is used to store the boolean in the storage indicating the container is initialized.
   */
  storageKey: string;
  /**
   * Used to store initialization status.
   */
  storage: KeyValueStorage<string, boolean>;
  /**
   * Used to store initialization status.
   */
  rootWebId: string;
  /**
   * Reference to the registration manager, used to register ROOT identity
   */
  registrationManager: RegistrationManager
 }

/**
 * Initializer that sets up a container.
 * Will copy all the files and folders in the given path to the corresponding documents and containers.
 */
export class RootContainerInitializer extends Initializer {
  protected readonly logger = getLoggerFor(this);

  private readonly store: ResourceStore;
  private readonly containerId: ResourceIdentifier;
  private readonly generator: ResourcesGenerator;
  private readonly storageKey: string;
  private readonly storage: KeyValueStorage<string, boolean>;
  private readonly rootWebId: string;
  private readonly registrationManager: RegistrationManager;

  public constructor(args: RootContainerInitializerArgs) {
    super();
    this.containerId = { path: ensureTrailingSlash(joinUrl(args.baseUrl, args.path)) };
    this.store = args.store;
    this.generator = args.generator;
    this.storageKey = args.storageKey;
    this.storage = args.storage;
    this.rootWebId = args.rootWebId;
    this.registrationManager = args.registrationManager;
  }

  public async handle(): Promise<void> {
    this.logger.info(`Initializing container ${this.containerId.path}`);
    const resources = this.generator.generate(this.containerId, {rootWebId: this.rootWebId});
    let count = 0;
    for await (const { identifier: resourceId, representation } of resources) {
      try {
        await this.store.setRepresentation(resourceId, representation);
        count += 1;
      } catch (error: unknown) {
        this.logger.warn(`Failed to create resource ${resourceId.path}: ${createErrorMessage(error)}`);
      }
    }
    this.logger.info(`Initialized ROOT container ${this.containerId.path} with ${count} resources.`);

    // register ROOT identity
    const url = new URL(this.rootWebId);
    const podName = url.pathname.split('/')[1];
    const rootRegistrationParams: RootRegistrationParams =  {
      t: 'root',
      name: 'Egendata',
      podName,
    };
    this.registrationManager.registerRoot(rootRegistrationParams, this.registrationManager.register);
    this.logger.info(`Registered egendata identity.`);


    // Mark the initialization as finished
    await this.storage.set(this.storageKey, true);
  }
}
