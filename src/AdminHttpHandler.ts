import { AccountStore, BadRequestHttpError, ClientCredentials, getLoggerFor, HttpHandler, HttpHandlerInput, HttpResponse, KeyValueStorage } from "@solid/community-server";
import { OpenAPIBackend } from 'openapi-backend';
import type { Request } from 'openapi-backend';
import { RegistrationManager, RegistrationParams, ServiceRegistrationParams, Template } from "./RegistrationManager";
import { ServerResponse } from "http";

interface RequestBody {
  name: string;
  podName: string;
  clientId: string;
  clientSecret: string;
  logo?: string;
  adminWebId: string;
}

const writeResponse = (resp: ServerResponse, code: number, data: any) => {
  resp.writeHead(code, {
    'content-type': 'application/json'
  });
  resp.end(JSON.stringify(data));      
}

const writeError = (resp: ServerResponse, error: string) => {
  resp.writeHead(400, {
    'content-type': 'application/json'
  });
  resp.end(JSON.stringify({error}));      
}

export class AdminHttpHandler extends HttpHandler {
  protected readonly logger = getLoggerFor(this);
  protected readonly api: OpenAPIBackend;
  private accessKey: string;
  private accountStore: AccountStore;
  private credentialStorage: KeyValueStorage<string, ClientCredentials>;
  private registrationManager: RegistrationManager;

  public constructor(
    accessKey: string,
    accountStore: AccountStore,
    credentialStorage: KeyValueStorage<string, ClientCredentials>,
    registrationManager: RegistrationManager,
    rootWebId: string,
  ) {
    super();
    this.accessKey = accessKey;
    this.accountStore = accountStore;
    this.credentialStorage = credentialStorage;
    this.registrationManager = registrationManager;

    this.api = new OpenAPIBackend({ definition: './AdminAPI.yaml' });
    this.api.register({
      addServiceUser: async (c, req, resp) => {
        const headers = c.request.headers;
        const key = headers['x-auth-key'];
        if (key !== this.accessKey) {
          throw new BadRequestHttpError("Bad access token provided")
        }
        const body: RequestBody = c.request.requestBody;
        this.logger.info(`\npodName = ${body.podName}`);

        const registrationParams: ServiceRegistrationParams = {
          t: 'service',
          name: body.name,
          podName: body.podName,
          clientId: body.clientId,
          clientSecret: body.clientSecret,
          logo: body.logo,
          adminWebId: body.adminWebId,
        };

        const regman: RegistrationManager = this.registrationManager;

        try {
          try {

            const registrationResponse = await regman.register(registrationParams, true);
            if (!registrationResponse.webId) {
              writeError(resp, `Could not create account with podName ${body.podName}.`);

              return;
            }

            const settings = await this.accountStore.getSettings(registrationResponse.webId);
    
            const clientId = registrationParams.clientId;
            const clientSecret = registrationParams.clientSecret;
            settings.clientCredentials = settings.clientCredentials ?? [];
            const clientCredentialsSet = new Set(settings.clientCredentials);
            if (clientId && clientSecret) {
              clientCredentialsSet.add(clientId);
              settings.clientCredentials = [...clientCredentialsSet];
              await this.accountStore.updateSettings(registrationResponse.webId, settings);
              await this.credentialStorage.set(clientId, { secret: clientSecret, webId: registrationResponse.webId });
            }
            const retval = {
              name: registrationParams.name,
              webId: registrationResponse.webId,
            }
            writeResponse(resp, 201, retval);
          }
          catch(error: any) {
            this.logger.error(`could not register: ${error.message}`);
            writeError(resp, error.message);
            return;
          };

        } catch (error: any) {
          writeError(resp, error.message);
          return;
        }
      },
      validationFail: (err, req, res) => {
        const message = err.validation.errors![0].message!;
        writeError(res, `Validation Error: ${message}`);
      }
    });
    
    // initalize the backend
    this.api.init();
  }

  public async canHandle(input: HttpHandlerInput): Promise<void> {
    await super.canHandle(input);
    if (input.request.method !== 'POST') {
      throw new BadRequestHttpError();
    }
  }

  public async handle({ request, response }: HttpHandlerInput): Promise<void> {
    const body = await request.read() ?? '';
    const url = request.url !== undefined ? request.url : '';
    const replacedUrl = url.replace('/.admin/', '/');
    const path = url.startsWith('/.admin/') ? replacedUrl :  url;

    const apiRequest: Request = {
      method: request.method !== undefined ? request.method : '',
      // Rewrite requests to allow hosting API on root paths
      path,
      headers: request.headers as any,
      query: '',
      body: body.toString(),
    }

    await this.api.handleRequest(apiRequest, request, response);
  }
}
