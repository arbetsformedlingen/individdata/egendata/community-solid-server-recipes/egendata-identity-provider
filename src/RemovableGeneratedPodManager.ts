import { cloneRepresentation, GeneratedPodManager, getLoggerFor, INTERNAL_QUADS, isContainerIdentifier, PodManager, ResourceIdentifier, ResourcesGenerator, ResourceStore } from '@solid/community-server';
import { Store } from 'n3';

export interface RemovablePodManager extends PodManager {
  deletePod: (identifier: ResourceIdentifier) => Promise<void>,
}

/**
 * Pod manager that uses an {@link IdentifierGenerator} and {@link ResourcesGenerator}
 * to create the default resources and identifier for a new pod.
 */
export class RemovableGeneratedPodManager extends GeneratedPodManager implements RemovablePodManager{
  protected readonly logger = getLoggerFor(this);

  private readonly storeRef: ResourceStore;

  public constructor(store: ResourceStore, resourcesGenerator: ResourcesGenerator) {
    super(store, resourcesGenerator);
    this.storeRef = store;
  }

  /**
   * Deletes the pods resources recursively.
   */
  public async deletePod(identifier: ResourceIdentifier) {
    const recursiveDeleteResource = async (resource: ResourceIdentifier) => {
      const representation = await cloneRepresentation(await this.storeRef.getRepresentation(resource, { type: { [INTERNAL_QUADS]: 1 } }));
      const quads = representation.metadata.quads();
  
      const n3Store = new Store(quads);
      const contains = n3Store.getQuads(null, 'http://www.w3.org/ns/ldp#contains', null, null);

      for (const containedQuad of contains) {
        const containedResourceIdentifier = { path: containedQuad.object.value };
        if (isContainerIdentifier(containedResourceIdentifier)) {
          // Recursive deletion
          await recursiveDeleteResource(containedResourceIdentifier);
          // Delete container
          await this.storeRef.deleteResource(containedResourceIdentifier);
          console.log('Deleted container:', containedResourceIdentifier);
        } else {
          // Delete resource from store
          await this.storeRef.deleteResource(containedResourceIdentifier);
          console.log('Deleted resource:', containedResourceIdentifier);
        }
      }
    }

    await recursiveDeleteResource(identifier);
  }
}
