/**
 * User account information
 */
 export interface AccountInfo {
  uuid: string;
  webId: string;
}

/**
 * Options that can be set on an account.
 */
export interface AccountSettings {
  /**
   * If this account can be used to identify as the corresponding WebID in the IDP.
   */
  useIdp: boolean;
  /**
   * The base URL of the pod associated with this account, if there is one.
   */
  podBaseUrl?: string;
  /**
   * All credential tokens associated with this account.
   */
  clientCredentials?: string[];
  /**
   * If storage has been created on the pod server.
   */
  storageCreated: boolean;
  /**
   * Storage uuid (podname) on the pod server.
   */
  podStorage?: string;
}

/**
 * Storage needed for the ssn interaction
 */
export interface AccountStore {
  /**
   * Get account information for the given ssn.
   * @param ssn - The user's ssn.
   * @returns the account information or undefined if the account doesn't exist.
   */
  getAccountInfo: (ssn: string) => Promise<AccountInfo | undefined>;

  /**
   * Creates a new account.
   * @param ssn - Account ssn.
   * @param webId - Account WebID.
   * @param uuid - Account uuid.
   * @param settings - Specific settings for the account.
   */
  create: (ssn: string | undefined, webId: string, uuid: string, settings: AccountSettings) => Promise<void>;

  /**
   * Gets the settings associated with this account.
   * Errors if there is no matching account.
   * @param webId - The account WebID.
   */
  getSettings: (webId: string) => Promise<AccountSettings>;

   /**
   * Updates the settings associated with this account.
   * @param webId - The account WebID.
   * @param settings - New settings for the account.
   */
  updateSettings: (webId: string, settings: AccountSettings) => Promise<void>;

  /**
   * Delete the account.
   * @param ssn - The user's ssn.
   */
  deleteAccount: (ssn: string | undefined, webId: string) => Promise<void>;
}
