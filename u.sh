#!/bin/sh
set -e

#ADMIN_URL="https://idp-test.jobtechdev.se/.admin/serviceUser"
ADMIN_URL="http://localhost:3002/.admin/serviceUser"
#ADMIN_URL="https://idp-test.egendata.se/.admin/serviceUser"

#B64LOGO=`base64 -w 0 $6`
B64LOGO=`openssl base64 -A -in $6`

LOGOMIME=`file --mime-type -b $6`

LOGO="data:$LOGOMIME;base64,$B64LOGO"

PAYLOAD="{\"podName\": \"$2\", \"name\": \"$3\", \"clientId\": \"$4\", \"clientSecret\": \"$5\", \"logo\": \"$LOGO\", \"adminWebId\": \"$7\", \"template\": $8}"

curl -s "$ADMIN_URL" \
  -H 'Expect:' \
  -H 'Accept: application/json' \
  -H 'X-Auth-Key: '$1 \
  -H 'Content-Type: application/json' \
  -d "$PAYLOAD"
