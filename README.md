# Egendata Identity Provider

## Start

```
$ CSS_VISMA_API_URL=https://idp-test.ciceron.cloud CSS_VISMA_CUSTOMER_KEY=[CUSTOMER_KEY] CSS_VISMA_SERVICE_KEY=[SERVICE_KEY] CSS_POD_STORAGE_BASE_URL= --podStorageBaseUrl=http://localhost:3001/ CSS_CLIENT_ID=my-client-id CSS_CLIENT_SECRET=my-client-secret CSS_PORT=3002 CSS_BASE_URL=http://localhost:3002 npx community-solid-server -c config/config.json -m . -f store -r http://localhost:3002/000000000000/profile/card#me -a [ADMIN_KEY]
```
