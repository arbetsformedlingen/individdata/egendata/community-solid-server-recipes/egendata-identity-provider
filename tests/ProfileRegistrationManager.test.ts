import {afterEach, beforeAll, describe, expect, it, jest} from '@jest/globals';
import { AccountInfo, AccountStore } from '../src/AccountStore';
import { ProfilePodSettings } from '../src/ProfilePodSettings';
import { AuthFetchFunction, FetchFunction } from '../src/AuthFetchFactory';
import {ProfileRegistrationManager, ProfileRegistrationManagerArgs} from '../src/ProfileRegistrationManager';
import { RegisterFunction, RegistrationManager } from '../src/RegistrationManager';
import { RemovablePodManager } from '../src/RemovableGeneratedPodManager';
import { AlgJwk, ClientCredentials, HttpError, IdentifierGenerator, JwkGenerator, KeyValueStorage, PodSettings, ResourceIdentifier } from '@solid/community-server';
import { RequestInit, Response } from "node-fetch";

afterEach(() => {
  //jest.resetAllMocks();
})

const rootWebId = 'https://idp/root/profile/card#me';

const notRegistered = jest.fn(id => Promise.resolve(undefined));

const identifierGeneratorMock: IdentifierGenerator = {
  generate: jest.fn(name => ({ path: `https://idp/${name}` })),
  extractPod: function (identifier: ResourceIdentifier): ResourceIdentifier {
    throw new Error('Function not implemented.');
  }
};

const accountStoreMock: AccountStore = {
  getAccountInfo: notRegistered,
  create: jest.fn((ssn, webId, uuid, settings) => Promise.resolve()),
  getSettings: jest.fn(webId => Promise.resolve({ useIdp: true, storageCreated: true})),
  updateSettings: jest.fn((webId, settings) => Promise.resolve()),
  deleteAccount: jest.fn((ssn: string | undefined, webId: string) => Promise.resolve()),
};

const asyncIterableIterator: AsyncIterableIterator<[string, ClientCredentials]> = {
  async next() {
      return Promise.resolve({
        done: true,
        value: undefined,
      });
    },
  [Symbol.asyncIterator]() {
      return this;
  }
}

const credentialStorageMock: KeyValueStorage<string, ClientCredentials> = {
  get: jest.fn(key => Promise.resolve({secret: '', webId: ''})),
  has: jest.fn(key => Promise.resolve(true)),
  set: jest.fn((key, value) => Promise.resolve(credentialStorageMock)),
  delete: jest.fn(key => Promise.resolve(true)),
  entries: jest.fn(() => asyncIterableIterator),
}

const removablePodManagerMock: RemovablePodManager = {
  createPod: jest.fn((identifier, settings: ProfilePodSettings, overwrite) => {
    if (settings.template === '409') {
      return Promise.reject(new HttpError<409>(409, ''));
    } else if (settings.template === '400') {
      return Promise.reject(new HttpError<400>(400, ''));
    }
    return Promise.resolve();
  }),
  deletePod: jest.fn((identifier) => Promise.resolve()),
}

const jwkGeneratorMock: JwkGenerator = {
  alg: 'ES256',
  getPrivateKey: jest.fn((): Promise<AlgJwk> => Promise.resolve({
    kty: 'EC',
    d: 'nqQvia4yP6eavzXv4tehVe19N3e5JPoVux0HnYsmnIE',
    use: 'sig',
    crv: 'P-256',
    kid: 'sig-2022-11-21T19:40:45Z',
    x: '_aIgxP3I-z10NKs2PNd3n94M9bFhitV0AGBzwNrgEyI',
    y: 'nMBDN37cpyjDPPIFet4WAsiI0KaNW1s35IO0sl64tsw',
    alg: 'ES256'
})),
  getPublicKey: jest.fn((): Promise<AlgJwk> => Promise.resolve({
    kty: 'EC',
    use: 'sig',
    crv: 'P-256',
    kid: 'sig-2022-11-21T19:40:45Z',
    x: '_aIgxP3I-z10NKs2PNd3n94M9bFhitV0AGBzwNrgEyI',
    y: 'nMBDN37cpyjDPPIFet4WAsiI0KaNW1s35IO0sl64tsw',
    alg: 'ES256'
})),
}

const responseMock = jest.mocked(Response);

const fetchFuncionMock: FetchFunction = (input) => {
  const response: Response = new responseMock;
  return Promise.resolve(response);
}

const fetchFuncionRejectMock: FetchFunction = (input) => {
  return Promise.reject(new Error('reject'));
}

const fetchFuncionExtraMock: FetchFunction = (input, init: RequestInit | undefined) => {
  if (init!.method === 'GET') {
    return Promise.reject();
  }
  const response: Response = new responseMock;
  return Promise.resolve(response);
}

const authFetchFactoryMock = {
  getAuthFetch: jest.fn((id, secret) => {
   return Promise.resolve(fetchFuncionMock);
  }),
}

const authFetchFactoryRejectMock = {
  getAuthFetch: jest.fn((id, secret) => {
   return Promise.resolve(fetchFuncionRejectMock);
  }),
}

const authFetchFactoryExtraMock = {
  getAuthFetch: jest.fn((id, secret) => {
   return Promise.resolve(fetchFuncionExtraMock);
  }),
}

/*
const responseMock = jest.mocked(Response);

const fetchFuncionMock: FetchFunction = (input) => {
  const response: Response = new responseMock;
  return Promise.resolve(response);
}

const fetchFuncionRejectMock: FetchFunction = (input) => {
  return Promise.reject(new Error('reject'));
}

const fetchFuncionExtraMock: FetchFunction = (input, init: RequestInit | undefined) => {
  if (init!.method === 'GET') {
    return Promise.reject();
  }
  const response: Response = new responseMock;
  return Promise.resolve(response);
}

const authFetchFactoryMock = {
  getAuthFetch: jest.fn((id, secret) => {
   return Promise.resolve(fetchFuncionMock);
  }),
}

const authFetchFactoryRejectMock = {
  getAuthFetch: jest.fn((id, secret) => {
   return Promise.resolve(fetchFuncionRejectMock);
  }),
}

const authFetchFactoryExtraMock = {
  getAuthFetch: jest.fn((id, secret) => {
   return Promise.resolve(fetchFuncionExtraMock);
  }),
}
*/

describe('ProfileRegistrationManager Idp registration', () => {

  const idpRegistered = jest.fn(async (id: string) => {
    if (id === '000000000000') {
      return {
        uuid: '000000000000',
        webId: 'https://idp/000000000000/profile/card#me',
      }
    }
    return undefined;
  })

  const args: ProfileRegistrationManagerArgs = {
    rootWebId,
    baseUrl: 'https://idp/',
    webIdSuffix: '/profile/card#me',
    identifierGenerator: identifierGeneratorMock,
    accountStore: accountStoreMock,
    rootPodManager: removablePodManagerMock,
    servicePodManager: removablePodManagerMock,
    personPodManager: removablePodManagerMock,
    podStorageBaseUrl: 'https://pod/',
    credentialStorage: credentialStorageMock,
    clientId: 'clientId',
    clientSecret: 'clientSecret',
    jwkGenerator: jwkGeneratorMock,
    authFetchFactory: authFetchFactoryMock
  }

  it('should register idp account if not present', async () => {
    accountStoreMock.getAccountInfo = notRegistered;

    const registrationManager = new ProfileRegistrationManager(args);

    await registrationManager.registerRoot({
      t: 'root',
      name: 'Root',
      podName: 'root',
    });

    expect(accountStoreMock.getSettings).toHaveBeenCalledWith('https://idp/root/profile/card#me');
    expect(accountStoreMock.updateSettings).toHaveBeenCalledWith('https://idp/root/profile/card#me', {'clientCredentials': ['clientId'], 'storageCreated': true, 'useIdp': true});
    expect(credentialStorageMock.set).toBeCalledWith('clientId', {'secret': 'clientSecret', 'webId': 'https://idp/root/profile/card#me'});
  })

})

describe('ProfileRegistrationManager', () => {

  const args: ProfileRegistrationManagerArgs = {
    rootWebId,
    baseUrl: 'https://idp/',
    webIdSuffix: '/profile/card#me',
    identifierGenerator: identifierGeneratorMock,
    accountStore: accountStoreMock,
    rootPodManager: removablePodManagerMock,
    servicePodManager: removablePodManagerMock,
    personPodManager: removablePodManagerMock,
    podStorageBaseUrl: 'https://pod/',
    credentialStorage: credentialStorageMock,
    clientId: 'clientId',
    clientSecret: 'clientSecret',
    jwkGenerator: jwkGeneratorMock,
    authFetchFactory: authFetchFactoryMock
  }

  let manager: RegistrationManager;
  beforeAll(() => {
    manager = new ProfileRegistrationManager(args);
  });
  
  it('should create a root pod', async () => {

    const registerFunction: RegisterFunction = jest.fn<RegisterFunction>(async (input) => await manager.register(input));
    await manager.registerRoot({
      t: 'root',
      name: 'Root',
      podName: 'root',
    }, registerFunction);
    expect(identifierGeneratorMock.generate).toHaveBeenCalled();
    expect(accountStoreMock.create).toBeCalledWith(undefined, 'https://idp/root/profile/card#me', 'root', {
      podBaseUrl: "https://idp/root",
      podStorage: undefined,
      storageCreated: false,
      useIdp: true,
    });
  })

  it('should not throw error if pod creation results in 409', async () => {
    // given root pod already have been created
    removablePodManagerMock.createPod = jest.fn((identifier, settings, overwrite) => Promise.reject(new HttpError<409>(409, '')));

    const registrationResponsePromise = manager.register({
      t: 'root',
      name: 'Root',
      podName: 'root',
    }, false);
    expect(identifierGeneratorMock.generate).toHaveBeenCalled();
    expect(registrationResponsePromise).resolves.toStrictEqual({
      accountCreated: false,
      oidcIssuer: 'https://idp/',
      personalNumber: undefined,
      podBaseUrl: 'https://idp/root',
      webId: 'https://idp/root/profile/card#me'
    });
  })

  it('should throw error if pod creation throws', async () => {
    //removablePodManagerMock.createPod = jest.fn((identifier, settings, overwrite) => Promise.reject(new HttpError<400>(400, '')));

    try {
      const registrationResponsePromise = manager.register({
        t: 'person',
        firstName: 'Test',
        lastName: 'Person',
        personalNumber: '6127846712',
        podName: 'test1',
      }, false);
      expect(identifierGeneratorMock.generate).toHaveBeenCalled();
      expect(registrationResponsePromise).rejects;
    } catch (e: any) {
      expect(e).toStrictEqual(Error)
    }
  })

  it('should create user account', async () => {

    const args: ProfileRegistrationManagerArgs = {
      rootWebId,
      baseUrl: 'https://idp/',
      webIdSuffix: '/profile/card#me',
      identifierGenerator: identifierGeneratorMock,
      accountStore: accountStoreMock,
      rootPodManager: removablePodManagerMock,
      servicePodManager: removablePodManagerMock,
      personPodManager: removablePodManagerMock,
      podStorageBaseUrl: 'https://pod/',
      credentialStorage: credentialStorageMock,
      clientId: 'clientId',
      clientSecret: 'clientSecret',
      jwkGenerator: jwkGeneratorMock,
      authFetchFactory: authFetchFactoryMock
    };

    (args.accountStore.getAccountInfo as jest.Mock<(ssn: string) => Promise<AccountInfo | undefined>>).mockResolvedValueOnce({uuid: '823597203', webId: 'hgiheier'});

    const manager = new ProfileRegistrationManager(args);
    const registrationResponse = await manager.register({
      t: 'person',
      firstName: 'Test',
      lastName: 'Person',
      personalNumber: '6127846712',
      podName: 'test1',
    });
    expect(identifierGeneratorMock.generate).toHaveBeenCalled();
    expect(registrationResponse).toStrictEqual({
      accountCreated: false,
      oidcIssuer: "https://idp/",
      personalNumber: "6127846712",
      podBaseUrl: "https://idp/test1",
      webId: "https://idp/test1/profile/card#me"
    });
  })

  it('should create service account', async () => {

    (args.accountStore.getAccountInfo as jest.Mock<(ssn: string) => Promise<AccountInfo | undefined>>).mockResolvedValueOnce({uuid: '823597203', webId: 'hgiheier'});

    const manager = new ProfileRegistrationManager(args);
    const podName = 'service1';
    const clientId = 'clientId';
    const clientSecret = 'clientSecret';
    const registrationResponse = await manager.register({
      t: 'service',
      name: 'Test Service',
      podName,
      clientId,
      clientSecret
    });
    expect(identifierGeneratorMock.generate).toHaveBeenCalled();
    expect(registrationResponse).toStrictEqual({
      accountCreated: false,
      oidcIssuer: "https://idp/",
      personalNumber: undefined,
      podBaseUrl: "https://idp/service1",
      webId: "https://idp/service1/profile/card#me"
    });
  })

  it('should throw error if podManager throws error at pod creation', async () => {

    (args.accountStore.getAccountInfo as jest.Mock<(ssn: string) => Promise<AccountInfo | undefined>>).mockResolvedValueOnce({uuid: '823597203', webId: 'hgiheier'});
    (args.servicePodManager.createPod as jest.Mock<(identifier: ResourceIdentifier, settings: PodSettings, overwrite: boolean) => Promise<void>>).mockRejectedValueOnce(new Error('create error'));

    const manager = new ProfileRegistrationManager(args);
    const podName = 'service1';
    const clientId = 'clientId';
    const clientSecret = 'clientSecret';
    const registrationResponsePromise = manager.register({
      t: 'service',
      name: 'Test Service',
      podName,
      clientId,
      clientSecret
    });
    expect(identifierGeneratorMock.generate).toHaveBeenCalled();
    expect(registrationResponsePromise).rejects.toThrowError();
  })

  it('should log error if storage pod could not be created', async () => {

    (args.accountStore.getAccountInfo as jest.Mock<(ssn: string) => Promise<AccountInfo | undefined>>).mockResolvedValueOnce({uuid: '823597203', webId: 'hgiheier'});
    args.authFetchFactory = authFetchFactoryRejectMock;

/*     const logError = jest.fn();

    jest.mock('pino', () => {
      return {
        transport: jest.fn(), pino: jest.fn(() => {
          return {
            error: logError,
            }
        })
      };
    });
 */    

    const manager = new ProfileRegistrationManager(args);
    const podName = 'service1';
    const clientId = 'clientId';
    const clientSecret = 'clientSecret';
    const registrationResponsePromise = manager.register({
      t: 'service',
      name: 'Test Service',
      podName,
      clientId,
      clientSecret
    });

    // TODO: expect logger error to be called ...
    // how to mock pino logger?

    // logic dependency on myID and mySecret in the createStorage method is not a good idea since it's so hard to get the code tested.
    // rewrite with a boolean variable that can be set for the tests
   
 })

//});

//describe('createStorage', () => {

  it('should create pod on storage server', async () => {
    const authFetch = jest.fn();
    const webId = "https://idp/test1/profile/card#me"
    const storagePodUrl = 'https://idp/123456'

    const manager = new ProfileRegistrationManager(args);

    await manager.createStorageImpl('root', webId, storagePodUrl, authFetch as AuthFetchFunction);

    expect(authFetch).toHaveBeenCalledTimes(1);
  })

});

/*

  it('should create pod on storage server, adding logo if present', async () => {

    (args.accountStore.getAccountInfo as jest.Mock<(ssn: string) => Promise<AccountInfo | undefined>>).mockResolvedValueOnce({uuid: '823597203', webId: 'hgiheier'});

    //args.authFetchFactory = authFetchFactoryExtraMock;
    const manager = new ProfileRegistrationManager(args);
    const podName = 'test2';
    //const webId = manager.generateWebId(podName);
    const registrationResponse = await manager.register({
      personalNumber: '672318406',
      podName,
      logo: 'eradvadvdaggd',
      register: false,
      createPod: true,
      rootPod: false,
      createWebId: false
    });
    expect(identifierGeneratorMock.generate).toHaveBeenCalled();
    expect(registrationResponse).toStrictEqual({accountCreated: true});
  })

  it('should create pod on storage server, adding templates container if adminWebId present', async () => {

    (args.accountStore.getAccountInfo as jest.Mock<(ssn: string) => Promise<AccountInfo | undefined>>).mockResolvedValueOnce({uuid: '823597203', webId: 'hgiheier'});

    //args.authFetchFactory = authFetchFactoryExtraMock;
    const manager = new ProfileRegistrationManager(args);
    const podName = 'test2';
    //const webId = manager.generateWebId(podName);
    const registrationResponse = await manager.register({
      personalNumber: '672318406',
      podName,
      adminWebId: 'adminwebid',
      register: false,
      createPod: true,
      rootPod: false,
      createWebId: false
    });
    expect(identifierGeneratorMock.generate).toHaveBeenCalled();
    expect(registrationResponse).toStrictEqual({accountCreated: true});
  })

  it('should catch error if pod creation failes', async () => {

    (args.accountStore.getAccountInfo as jest.Mock<(ssn: string) => Promise<AccountInfo | undefined>>).mockResolvedValueOnce({uuid: '823597203', webId: 'hgiheier'});

    //args.authFetchFactory = authFetchFactoryRejectMock;
    const manager = new ProfileRegistrationManager(args);
    const podName = 'test2';
    //const webId = manager.generateWebId(podName);
    const registrationResponse = await manager.register({
      personalNumber: '672318406',
      podName,
      register: false,
      createPod: true,
      rootPod: false,
      createWebId: false
    });
    expect(identifierGeneratorMock.generate).toHaveBeenCalled();
    expect(registrationResponse).toStrictEqual({accountCreated: true});
  })

*/